<?php


include 'tartar.php';

$auth_token = '<API_TOKEN>';

// You may use file path or url
$file_path = 'path/to/your/file';
$url = 'www.awesomeurl.com';

$callback_url = 'http://callback.url/';

$objects = array(
		array(
			'strategy'=> 'exact',
			'width'=> 900,
			'height'=> 300,
		),
		array(
			'strategy'=> 'exact',
			'width'=> 800,
			'height'=> 500,
		)
	);

$s3_store = array(
		'key' => '<S3_API_KEY>',
		'secret' => '<S3_API_SECRET>',
		'bucket' => 's3_bucket_name'
		);

// file_upload function is used to upload images directly
// file_upload arguments:
// ($auth_token, $file_path, $s3_store=NULL, $objects=NULL, $objects_delay=NULL, $callback_url=NULL, $lossy=NULL)
$response = file_upload($auth_token, $file_path, $s3_store,  null, $objects, $callback_url, true);

// url_upload function is used to upload images from url
// url_upload arguments:
// ($auth_token, $url, $s3_store=NULL, $objects=NULL, $objects_delay=NULL, $callback_url=NULL, $lossy=NULL)
$response_url = url_upload($auth_token, $url, $s3_store,  null, $objects, $callback_url, true);


?>
