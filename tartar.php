<?php

function file_upload($auth_token, $file_path, $s3_store=NULL, $objects=NULL, $objects_delay=NULL, $callback_url=NULL, $lossy=NULL){
	$url = "https://tartar.io/upload/";

	$data = array();

	if (isset($s3_store)){
		$data['s3_store'] = $s3_store;
	}
	if (isset($objects)){
		$data['resize'] = $objects;
	}
	if (isset($objects_delay)){
		$data['resize_delay'] = $objects_delay;
	}
	if (isset($callback_url)){
		$data['callback_url'] = $callback_url;
	}
	if (isset($lossy) && $lossy == true){
		$data['lossy'] = true;
	}
	if (!isset($file_path)){
		return false;
	}

	$json_data = json_encode($data);
	$data = array('data' => $json_data);
	$data['file'] = new CurlFile($file_path);

	$curl = curl_init($url);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		'Content-Type: multipart/form-data',
		'Authorization: Token '.$auth_token
	));
	curl_setopt($curl, CURLOPT_HEADER, false);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

	$json_response = curl_exec($curl);

	curl_close($curl);

	$response = json_decode($json_response, true);
	return $response;
}

function url_upload($auth_token, $url, $s3_store=NULL, $objects=NULL, $objects_delay=NULL, $callback_url=NULL, $lossy=NULL){
	$c_url = "https://tartar.io/url/";

	$data = array();

	if (isset($s3_store)){
		$data['s3_store'] = $s3_store;
	}
	if (isset($objects)){
		$data['resize'] = $objects;
	}
	if (isset($objects_delay)){
		$data['resize_delay'] = $objects_delay;
	}
	if (isset($callback_url)){
		$data['callback_url'] = $callback_url;
	}
	if (isset($lossy) && $lossy == true){
		$data['lossy'] = true;
	}
	if (!isset($url)){
		return false;
	}
	$data['url'] = $url;

	$json_data = json_encode($data);
	$data = array('data' => $json_data);

	$curl = curl_init($c_url);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		'Content-Type: multipart/form-data',
		'Authorization: Token '.$auth_token
	));
	curl_setopt($curl, CURLOPT_HEADER, false);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $data );

	$json_response = curl_exec($curl);

	curl_close($curl);

	$response = json_decode($json_response, true);
	return $response;
}

?>
